'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Booking = require('../models/booking.js');


/*************************************************************************
 Function name  : create()
 parameters     : booking data
 model function call  : saveBooking
 Purpose        : This function is used for create day wise booking record 
 *************************************************************************/
exports.create = function(req, res, next) {
    Booking
        .saveBooking(req.body, function(err, saveBooking) {
            if (err) return res.status('500');
            if (!saveBooking) return next(new Error('Failed to load monthly billing '));
            return res.send({
                status : '200',
                booking: saveBooking    
            });
        });
};


/*************************************************************************
 Function name  : update()
 parameters     : id, bookingId
 model function call  : updateBooking
 Purpose        : This function is used for update booking record date and user
 *************************************************************************/

exports.update = function(req, res, next) {
    Booking
        .update( 
            { _id: req.body.id ,'booking.id': req.body.bookingId}, 
            { $set: { "photos.$.data": "yourdata" } })
        .exec(function(err, billing) {
            if (err) return next(err);
            if (!billing) return next(new Error('Failed to update billing'));
            return res.json(billing);
        });
};


/*************************************************************************
 Function name  : delete()
 parameters     : id, bookingId
 Purpose        : This function is used for update new booking record 
 *************************************************************************/
exports.delete = function(req, res, next) {

    Booking
        .update(
            {_id : req.params.id}, 
            { $pull: {'booking': {'id' : req.params.bookingId}}})
        .exec(function(err, booking) {
                console.log(booking);
                if (err) return next(err);
                if (!billing) return next(new Error('Failed to delete Billing'));
                return res.send({
                    status : '200',
                    booking: booking.booking
                });
            });
};

/*************************************************************************
 Function name  : list()
 model function call : bookingList
 parameters     : month, year
 Purpose        : get resources booking list for selected month and year
 *************************************************************************/
exports.list = function(req, res, next) {
    Booking
        .bookingList(req.params.month, req.params.year,function(err, bookingList) {
            
            if (err) return res.status('500');
            if (!bookingList) return next(new Error('Failed to load monthly booking'));
            return res.send({
                status : '200',
                period : bookingList.period,  
                bookings: bookingList.booking
            });
        });
};


/*************************************************************************
 Function name  : getbooking()
 model function call : getbooking
 parameters     : booking_id
 Purpose        : get booking data by id
 *************************************************************************/
exports.getBooking = function(req, res, next) {
    console.log(req.params.id);
    Booking
        .findOne({_id : req.params.id})
        .exec(function(err, booking) {
            if (err) return next(err);
            if (!booking) return next(new Error('Failed to load booking'));
            return res.send({
                status : '200',
                booking: booking.booking    
            });
        });
};



