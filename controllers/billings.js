'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Billing = require('../models/billing.js');

/*************************************************************************
 Function name  : list()
 Purpose        : This function is used for get list of billings
 *************************************************************************/
exports.list = function(req, res, next) {
    console.log('Test');
    Billing
        .find()
        .exec(function(err, billings) {
            if (err) return res.status('500');
            if (!billings) return res.send('data not present in database');
            return res.send({
                billings: billings    
            });
        });
};


/*************************************************************************
 Function name  : create()
 Purpose        : create new billing record
 *************************************************************************/
exports.create = function(req, res, next) {
    
    var date       =  new Date(req.body.date);
    req.body.month  =  date.getMonth()+1; 
    req.body.year =  date.getFullYear();
    var firstDay   =  new Date(date.getFullYear(), date.getMonth(), 1).getDay();
    req.body.week  =  Math.ceil((date.getDate() + firstDay)/7);    
    Billing
    	.create(req.body, function (err, billing) {
            if (err) return next(err);
            res.json(billing);
    	});
};

/*************************************************************************
 Function name  : update()
 Purpose        : Update billing record
 *************************************************************************/
exports.update = function(req, res, next) {
    var date       =  new Date(req.body.date);
    req.body.month  =  date.getMonth()+1; 
    req.body.year =  date.getFullYear();
    var firstDay   =  new Date(date.getFullYear(), date.getMonth(), 1).getDay();
    req.body.week  =  Math.ceil((date.getDate() + firstDay)/7);    
    delete req.body._id;
    Billing
        .findOneAndUpdate({_id:req.params.id}, req.body)
        .exec(function(err, billing) {
            if (err) return next(err);
            if (!billing) return next(new Error('Failed to update billing'));
            return res.json(billing);
        });
};

/*************************************************************************
 Function name  : delete()
 Purpose        : delete billing record
 *************************************************************************/
exports.delete = function(req, res, next) {
    Billing
        .findByIdAndRemove(req.params.id, req.body)
        .exec(function(err, billing) {
            if (err) return next(err);
            if (!billing) return next(new Error('Failed to delete Billing'));
            return res.json(billing);
        });
};

/**
 * get monthly billing
 */
exports.monthlyBilling = function(req, res, next) {
console.log('Test');
    Billing
        .monthlyBilling(req.params.month, req.params.year,function(err, monthlyBilling) {
            if (err) return res.status('500');
            if (!monthlyBilling) return next(new Error('Failed to load monthly billing '));
            return res.send({
                status : '200',
                billings: monthlyBilling    
            });
        });
};





