'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Master = require('../models/master');

/**
 *  get masters list
 */

exports.list = function(req, res, next) {
    Master
        .find()
        .exec(function(err, masters) {
            if (err) return res.status('500');
            if (!masters) return res.send('data not present in database');
            return res.send({
                masters: masters    
            });
        });
};


/**
 *  create new Billing
 */
exports.create = function(req, res, next) {
    Master
    	.create(req.body, function (err, master) {
            if (err) return next(err);
            res.json(master);
    	});
};

/**
 * update Billing by id
 */

exports.update = function(req, res, next) {
    delete req.body._id;
    Master
        .findOneAndUpdate({_id:req.params.id}, req.body)
        .exec(function(err, master) {
            if (err) return next(err);
            if (!master) return next(new Error('Failed to update master'));
            return res.json(master);
        });
};

/**
 * delete Billing by id
 */

exports.delete = function(req, res, next) {
    Master
        .findByIdAndRemove(req.params.id, req.body)
        .exec(function(err, master) {
            if (err) return next(err);
            if (!master) return next(new Error('Failed to delete master'));
            return res.json(master);
        });
};


/*************************************************************************
 Function name  : masterOptions()
 Purpose        : This function is used for get masters options
 model function call : masterOptions
 *************************************************************************/
exports.masterOptions = function(req, res, next) {
    
    Master.masterOptions(function(err, masterOptions) {
            if (err) return next(err);
            if (!masterOptions) return next(new Error('Failed to load'));
            return res.send({
            	status : '200',
                masters: masterOptions    
            });
        });
};


