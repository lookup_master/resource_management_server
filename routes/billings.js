var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var billings = require('../controllers/billings');

/* GET /Billings listing. */
router.get('/', billings.list);


/* Create  new Billing */
router.post('/', billings.create);

/* update Billing */
router.put('/:id', billings.update);

/* delete Billing */
router.delete('/:id', billings.delete);

/* GET resource booking list by month and year */
router.get('/monthly-booking/:month/:year', billings.monthlyBilling);




module.exports = router;
//masterData.push({'name':name1.name,'id':name1.id});