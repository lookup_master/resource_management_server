var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var bookings = require('../controllers/bookings');

/*  get day wise booking list for selected month and year */
router.get('/get-list/:month/:year', bookings.list);

/** get booking by _id **/
router.get('/get-booking/:id', bookings.getBooking);

/*  create booking listing. */
router.post('/save-booking', bookings.create);

/* update booking */
router.put('/:id/:bookingId', bookings.update);

/* delete booking */
router.delete('/:id/:bookingId', bookings.delete);


module.exports = router;
//masterData.push({'name':name1.name,'id':name1.id});