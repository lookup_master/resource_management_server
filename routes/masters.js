var express = require('express');
var router = express.Router();
var masters = require('../controllers/masters');

/**
* GET Master options for 
* list like resorces-name, status, technology, billing-modes 
 */
router.get('/', masters.list);

/*
* get master list
*/
router.get('/master-options', masters.masterOptions);

/* Create  new master */
router.post('/', masters.create);

/* update master record by id */
router.put('/:id', masters.update);

/* delete master record by id */
router.delete('/:id', masters.delete);


module.exports = router;
