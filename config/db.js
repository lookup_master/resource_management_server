var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/LookupMasterTest', function(err) {
    if(err) {
        console.log('connection error', err);
    } else {
        console.log('connection successful');
    }
});
exports.mongoose = mongoose;