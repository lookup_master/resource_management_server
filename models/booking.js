'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Master = require('../models/master');
var Period = require('../models/period');
var Billing = require('../models/billing');
var async = require('async');
var moment = require('moment');


/**
 * Billing Schema
 */
var BookingSchema = new mongoose.Schema({
    resource_name: {
        'name':String, 'id':String
    },
    total_hours : {
        type : Number
    },  
    booking_date: {
        type : Date,
    },  
    booking_month: {
        type : String,
    },
    booking_year: {
        type : String,
    },
    booking_day: {
        type : String,
    }, 
    total_efforts: {
        type : Number,
    },
    soft_delete: { 
        type: Number, 
        default:0
    },
    created_on: { 
        type: Date, 
      default: Date.now 
    },
    booking: Array

});

var Booking = mongoose.model('Booking', BookingSchema);

/*************************************************************************
 Function name  : saveBooking()
 parameters     : bookingData, callback
 Purpose        : This function is used for store booking data
                  1) new booking for day and user, 
                  2) if booking already exit for the same day and same user
 *************************************************************************/
Booking.saveBooking = function( record,  callback) {
    var count = 0,
        finalBooking = [],
        from_date = new Date(record.booking_date),
        resourceName = record.resource_name.id;
        var to_date = new Date(record.booking_date),
        i,
        j,
        rangeflag = false,
        totalHours = 0,
        totalHours1 =  0;
    var remainHr = 0; 
    var remainValue = 0;   
    
        from_date.toLocaleDateString();

            record.total_efforts    =  record.booking.hours;
            record.booking.id       =  1;
            record.booking          =  [record.booking];
            var date                =  new Date(record.booking_date);
            record.booking_month    =  date.getMonth()+1; 
            record.booking_year     =  date.getFullYear(); 
            var firstDay            =  new Date(date.getFullYear(), date.getMonth(), 1).getDay();
            record.week             =  Math.ceil((date.getDate() + firstDay)/7);    
            record.booking_day      =  new Date(record.booking_date).getDate();
            rangeflag = record.booking[0].range_booking;
      
          try{
               if(rangeflag) {
                var range = Math.floor(Number(record.booking[0].hours) / Number(record.booking[0].hours_per_day));
                to_date.setDate(to_date.getDate()+Number(range));
                to_date.toLocaleDateString();
                totalHours = record.booking[0].hours;
                totalHours1 = record.booking[0].hours
                remainHr = Number (record.booking[0].hours) % Number (record.booking[0].hours_per_day);
                var numIteration = Number(record.booking[0].hours) / Number(record.booking[0].hours_per_day); 
                numIteration = Math.floor(numIteration);
                if( !record.booking_id ) {
                  async.whilst(function () { 
                                return count <  Number(totalHours);
                            },
                            function (next) {
                                var date = new Date(record.booking_date);
                                var month = date.getMonth()+1;
                                var day = date.getDate();
                                var year = date.getFullYear();

                            
                                Booking.findOne({'booking_month':month,'booking_year':year,'booking_day':day
                                , 'resource_name.id': record.resource_name.id }).exec(function (err, bookingData) {
                                   
                                   if(bookingData) {
                                         if (Number(bookingData.total_efforts) < 8) {
                                            var hr = 8 - Number(bookingData.total_efforts);
                                         if (Number(hr) >= Number(record.booking[0].hours_per_day)) {
                                            record.booking[0].hours = Number(record.booking[0].hours_per_day);
                                            hr = Number(record.booking[0].hours);
                                         } else {
                                            record.booking[0].hours = Number(record.booking[0].hours_per_day) - Number(hr);
                                            hr = Number(record.booking[0].hours); 
                                        } 
                                         record.booking.id = bookingData.booking.length + Number(1);
                                         bookingData.booking.push(record.booking[0]);
                                         bookingData.total_efforts = (bookingData.total_efforts) + Number(record.booking[0].hours); 
                                         bookingData.save(function (err, booking) {
                                            if( err) {
                                                console.log("err1 ::"+err);
                                                return false
                                            } else {
                                                 count =  Number(count) +  Number(hr);
                                                 var date = new Date(record.booking_date);
                                                if (date.getDay() === 5) {
                                                    date.setDate(date.getDate()+3);
                                                    } else {
                                                    date.setDate(date.getDate()+1);
                                                }
                                        record.booking_day  =  new Date(record.booking_date).getDate()+1;
                                        record.booking_date = date;

                                                 next();
                                            }
                                        });

                                    }
                                         
                                   } else {
                                        if (Number(remainValue) === Number (numIteration))  {
                                        record.total_efforts    =  Number(totalHours1);
                                        record.booking[0].hours = Number(totalHours1);  
                                        } else {
                                            record.total_efforts    =  record.booking[0].hours_per_day;
                                            record.booking[0].hours = Number(record.booking[0].hours_per_day); 
                                        }         
                                        Booking.create(record, function (err, insertBooking) {
                                        
                                                 
                                            if(err) {
                                                return false;  
                                            }
                                            if(insertBooking) {                                          
                                                count = Number(count) + Number(record.booking[0].hours_per_day);
                                                totalHours1 = Number(totalHours1) - Number(record.booking[0].hours_per_day);

                                                remainValue = Number(remainValue) + 1; 
                                                
                                                var date = new Date(record.booking_date);
                                                if (date.getDay() === 5) {
                                                    date.setDate(date.getDate()+3);
                                                    record.booking_day = date.getDate();
                                                } 
                                                else {
                                                    date.setDate(date.getDate()+1);
                                                    record.booking_day = new Date(record.booking_date).getDate()+1;
                                                }
                                               
                                                record.booking_date = date;
                                                finalBooking.push(insertBooking);
                                            next();
                                        }                               
                            });  
                          }
                      });
                            },function (err) {
                                     callback(null, finalBooking[0]);                                   
                            });

                }
              else {
                    this.findOne({_id:record.booking_id}).exec(function (err, bookingData) {  
                    if(err) {
                        console.log("err ::"+err);
                        return false;
                    }
                    
                    if(Number(bookingData.total_efforts) < Number(8) ) {
                        var hr = 8 - Number(bookingData.total_efforts);
                         if ( Number(record.booking[0].hours) >= Number(hr)) {
                            if(Number(hr) === Number(record.booking[0].hours_per_day) ){
                               record.booking[0].hours = Number(record.booking[0].hours_per_day);
                               hr = Number(record.booking[0].hours); 
                            } else if (Number(hr) >= Number(record.booking[0].hours_per_day) ){
                                record.booking[0].hours = Number(record.booking[0].hours_per_day)
                               hr = Number(record.booking[0].hours); 
                            } else if(Number(hr) <= Number(record.booking[0].hours_per_day )) {
                               record.booking[0].hours =  Number(hr);;
                               hr = Number(record.booking[0].hours);
                            }
                         
                        } else {
                            record.booking[0].hours = Number(hr);
                            hr = Number(record.booking[0].hours); 
                        }
                        record.booking.id = bookingData.booking.length + Number(1); 
                        bookingData.booking.push(record.booking[0]);
                        bookingData.total_efforts = (bookingData.total_efforts) + Number(record.booking[0].hours); 
                        bookingData.save(function (err, booking) { 
                         if(err) {
                            console.log("err:::" + err);            
                            return false;
                          } else {   
                          finalBooking.push(booking);    
                                    count =  Number(count) +  Number(hr);
                                    if (count < Number(totalHours)) {
                                        var date = new Date(record.booking_date);
                                        date.setDate(date.getDate()+1);
                                        record.booking_date = date;
                                        record.booking_day  =  new Date(record.booking_date).getDate()+1;
                                        totalHours= totalHours - count;
                                        addBooking(count, record,callback, finalBooking,totalHours, totalHours1, numIteration - Number(1));
                                    }
                          }  
                        });
                    } else {
                    addBooking(count, record,callback,finalBooking,totalHours, totalHours1, numIteration);
                }                                
                 });
            }

        } else {
                   if(!record.booking_id) {
                    record.booking[0].hours = record.booking[0].hours;
                     this.create(record, function (err, booking) {
                            if(err) return false;
                        callback(null, booking); 
                   });
               } else {
                 this
                .findOne({_id:record.booking_id}).exec(function (err, bookingData) {
                    record.booking.id = bookingData.booking.length + Number(1);
                    bookingData.booking.push(record.booking[0]);
                    bookingData.total_efforts =  Number(bookingData.total_efforts) + Number(record.booking[0].hours);
                    bookingData.save(function (err, booking) { 
                         if(err) return false;
                        callback(null, booking); 
                    });
                 });
               }

            }  
          } catch (exception) {
        return callback(null, true);
    } 
};

Booking.updateBooking = function(callback) {

    try{
        
        this
            .findOne({_id:record.booking_id}).exec(function (err, bookingData) {
                record.booking.id = bookingData.booking.length + Number(1);
                bookingData.booking.push(record.booking);
                bookingData.total_efforts = (bookingData.total_efforts) + Number(record.booking.hours); 
                bookingData.save(function (err, booking) { 
                     if(err) return false;
                    callback(null, booking); 
                });
             });
                
    } catch (exception) {

        return callback(null, true);
    }
};


/*************************************************************************
 Function name  : bookingList()
 parameters     : bookingData, callback
 model function call  : getBookingList
 Purpose        : find the period from database for the given month and year
 *************************************************************************/

Booking.bookingList = function(month, year, callback) {
    try{
        Period
            .findOne(
                    {'month': month, 'year' : year },
                    {'from_date':true, 'to_date': true ,'no_of_days' : true},
                    function( err, period ){
                        if (err) return next(err);
                        if(period){
                             getBookingList(period, callback);
                        }else{
                            callback(null, true);
                        }
                       
                    })

    } catch (exception) {
        return callback(null, true);
    }
};


/*************************************************************************
 Function name  : getBookingList()
 call from      : bookingList
 parameters     : period, callback
 model function call  : resourceList
 Purpose        : get aggregate of resources in booking list of give period
 *************************************************************************/

function getBookingList(period, callback){

    var from_date = new Date(period.from_date);
    var to_date = new Date(period.to_date);
    to_date.setDate(to_date.getDate()+1);
    to_date.toLocaleDateString();
    try{
        Booking
            .aggregate(  
                    { $match: { 'booking_date': { $gte: from_date, $lte: to_date}  } },
                    { $sort: { booking_date: -1 } },
                    { $group: { _id: '$resource_name.id', 
                                total: { $sum: '$total_efforts' },
                                hours: { $sum: '$booking.hours' },
                                field: { 
                                    "$addToSet" : { 
                                        "resource" : "$resource_name",
                                        "booking_id" : "$_id", 
                                        "booking_date" : "$booking_date",
                                        "booking" : "$booking.hours"
                                        
                                    }
                                }
                            }
                     }, 
                    function (err, bookingList) {
                        resourceList(period,bookingList, callback);
                    }
                );
    } catch (exception) {
        return callback(null, true);
    }
}

/*************************************************************************
 Function name  : resourceList()
 call from      : getBookingList
 parameters     : period, bookingList, callback
 Purpose        : get resources from master collection and find actual efforts from billing
 *************************************************************************/

function resourceList(period, bookingList , callback){
    var bookingData = new Array();
    try{
        Master
            .find(
                    {'type':'resource', 'soft_delete' : false},
                    {'name':true}
                )
            .exec(function(err, resources ) {
                if(err) return false;
                var from_date = new Date(period.from_date);
                var to_date = new Date(period.to_date);
                to_date.setDate(to_date.getDate()+1);
                to_date.toLocaleDateString();
                
                Billing
                    .aggregate( 
                        { $match: { 'date': { $gte: from_date, $lte: to_date }  } },
                        { $group: { _id: '$resource_name.id', total: { $sum: '$hours' },
                                    field: { '$addToSet' : { 'resource' : '$resource_name', 'date' : '$date'}}}}, 
                        function (err, billingsList) {

                            var billingUserIds = new Array();
                            billingsList.forEach(function ( billing ) {
                                billingUserIds[billing._id] = billing.total
                            });

                            formatBookingList(period,bookingList,billingUserIds,resources,callback);
                           
                        }
                    );
            });
    } catch (exception) {
        return callback(null, true);
    }
}

/*************************************************************************
 Function name  : formatBookingList()
 call from      : resourceList
 parameters     : period, bookingList, billingUserIds, resources,callback
 Purpose        : format whole data and pass callback 
 *************************************************************************/

function formatBookingList(period, bookingList ,billingUserIds, resources, callback){

    var bookingData = new Array();
    var bookings    = new Array();
    try{
        var resourceIds = [];
        bookingList.forEach(function ( bookings ) {

            var booking_date = new Date(bookings.field[0].booking_date);
            var dayWiseData = new Array();

            bookings.field.forEach(function ( bookingTemp ) {
                var total_hours = 0;
                var booking_date = new Date(bookingTemp.booking_date);
                for( var i = 0; i < bookingTemp.booking.length; i++){
                    total_hours += Number(bookingTemp.booking[i]);
                }
                dayWiseData.push({
                    hours :total_hours,
                    day :booking_date.getDate(),
                    month :booking_date.getMonth()+1,
                    booking_id:bookingTemp.booking_id

                });
                
            });
            
            if (billingUserIds[bookings.field[0].resource.id]) {
                var total_effort = billingUserIds[bookings.field[0].resource.id];
            }else{
                var total_effort = 0;
            }
            bookingData.push({
                resource_name: bookings.field[0].resource,
                estimated_efforts: bookings.total,
                actual_efforts: total_effort,
                days: dayWiseData
            });
            resourceIds.push(bookings._id);
            
        });
       
        resources.forEach(function ( resource ) {
            if( resourceIds.indexOf(resource.id) == -1){
                if (billingUserIds[resource._id]) {
                    var total_effort = billingUserIds[resource._id];
                }else{
                    var total_effort = 0;
                }
                bookingData.push({
                    resource_name:{ id : resource._id, name : resource.name },
                    estimated_efforts: 0,
                    actual_efforts: total_effort
                    //days : false
                });
            }
        });  
        bookings['period'] = period;
        bookings['booking'] = bookingData;
        callback(null, bookings);      
    } catch (exception) {
        console.log(exception);
        return callback(null, true);
    }
}

function addBooking (count, record, callback,finalBooking,totalHours, totalHours1, numIteration) {
    
    var hr;
    var day;
    var resBooking = record.booking[0].hours;
    var remainHr; 
    var remainValue = 0;
    
    
    if (count < Number(totalHours1)) {       
        async.whilst(function () {  
          return count <  Number(totalHours1);
        },
        function (next) {
            var date = new Date(record.booking_date),
                month = date.getMonth()+1,
                year= date.getFullYear(),
                day = date.getDate();         
                                       
                Booking.findOne({'booking_month':month,'booking_year':year,'booking_day':day
                            , 'resource_name.id': record.resource_name.id }).exec(function (err, bookingData) { 
                        if(err){
                           console.log("err1 ::" + err);
                            return false;  
                        } else if (bookingData && bookingData != null && Number(bookingData.total_efforts) < 8 
                            ) {
                            //bookingData.booking_date = bookingData.booking_date;
                            

                             hr = 8 - Number(bookingData.total_efforts);
                            if(Number(totalHours) >= Number(hr)) {
                                if (Number(hr) >= record.booking[0].hours_per_day) {
                                    record.booking[0].hours = record.booking[0].hours_per_day;
                                    hr = Number(record.booking[0].hours);
                                    totalHours = Number(totalHours) - Number(hr);
                                } else {
                                    record.booking[0].hours = Number(hr);
                                    hr = Number(record.booking[0].hours); 
                                    totalHours = Number(totalHours) - Number(hr);
                                }
                               
                            } else {
                                if (Number(hr) >= record.booking[0].hours_per_day)  {
                                    //record.booking[0].hours = Number(totalHours);
                                    if(Number(totalHours) <= Number(record.booking[0].hours_per_day)) {
                                      record.booking[0].hours = Number(totalHours);
                                      hr = Number(totalHours); 
                                    } else {
                                      record.booking[0].hours = Number(record.booking[0].hours_per_day);
                                      hr = Number(record.booking[0].hours); 
                                    /*newly added*/
                                    totalHours = Number(totalHours) - Number(hr);
                                    }
                                    
                                } else {
                                    record.booking[0].hours = Number(totalHours);
                                    hr = Number(record.booking[0].hours); 
                                }
                                
                            }

                            record.booking.id = bookingData.booking.length + Number(1); 
                            
                            bookingData.booking.push(record.booking[0]);
                            bookingData.total_efforts = (bookingData.total_efforts) + Number(record.booking[0].hours); 
                            bookingData.save(function (err, booking) {
                            if(err) {
                                 console.log("errr:"+err);
                                    return false;
                            }
                            if(booking){
                                    count = Number(count) + Number(hr);
                                    var date = new Date(record.booking_date);
                                    if (date.getDay() === 5) {
                                                    date.setDate(date.getDate()+3);
                                                } 
                                                else {
                                                    date.setDate(date.getDate()+1);
                                                }    
                                    record.booking_date = date;
                                    //finalBooking=booking;
                                    next();                                   
                            }
                                                      
                             });                         
                        } else {  
                                if (Number(remainValue) === Number (numIteration))  {
                                        record.total_efforts    =  Number(totalHours);
                                        record.booking[0].hours = Number(totalHours);  
                                        } else {
                                            record.total_efforts    =  record.booking[0].hours_per_day;
                                            record.booking[0].hours = Number(record.booking[0].hours_per_day); 
                                        }
                                var date = new Date(record.booking_date);
                                if (date.getDay() === 6) {
                                             date.setDate(date.getDate()+2);
                                        } else {
                                            date.setDate(date.getDate()+1);
                                        }
                                record.booking_day =  new Date(record.booking_date).getDate();

                                Booking.create(record, function (err, booking) {
                                     if(err) {
                                     console.log("error ::"+err);
                                        return false;  
                                    }
                                    if (booking){
                                        count = Number(count) + Number(record.booking[0].hours_per_day);
                                        totalHours = Number(totalHours) - Number(record.booking[0].hours_per_day);
                                        remainValue = Number(remainValue) + 1;
                                        var date = new Date(record.booking_date);
                                         if (date.getDay() === 5) {
                                                    date.setDate(date.getDate()+3);
                                                } 
                                                else {
                                                    date.setDate(date.getDate()+1);
                                                }    
                                        record.booking_date = date;
                                        next();
                                    }
                            });
                           }  
                          }); 
                        },
                        function (err) {
                                callback(null, finalBooking[0]);
                    } ); 
  }

                                                     
}

module.exports = Booking;