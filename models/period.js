'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * period Schema
 */
var PeriodSchema = new mongoose.Schema({
  	from_date: {
  	 	'type':String
    },
    to_date : {
        type : String
    },
    month : {
        type : String
    },
    year : {
        type : String
    }

});

module.exports = mongoose.model('Period', PeriodSchema);