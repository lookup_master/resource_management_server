'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');

/**
 * Task Schema
 */
var MasterSchema = new mongoose.Schema({
	name: { 
  		type: String
  	},
  	type: { 
  		type: String
  	},
  	status: { 
  		type: String
  	},
  	additional_info: { 
  		type: String
  	},
  	soft_delete: { 
  		type: Boolean,
  		default:false
  	},
  	created_on: { 
  		type: Date, 
        default: Date.now 
  	}
});

/*************************************************************************
 Function name  : masterOptions()
 parameters     : callback
 Purpose        : This function is used for get masters options
 *************************************************************************/
MasterSchema.statics.masterOptions = function( callback ){
	try{
	 	this
	 		.aggregate( 
	 			{ $match: { 'soft_delete': false, 'status' : 'active' } },
	 			{ $sort: { 'name': -1 } },
			    { $group: { _id: '$type', total: { $sum: 1 },
					options: { "$addToSet" : { "name" : "$name", "id" : "$_id"}}}}, 
		    	function (err, masterList) {
		    		if( masterList ){
			    		callback(null, masterList);
			    	}else{
			    		callback(err, false);
		    		}
			    }
			);
	} catch (exception) {
        return callback(null, true);
    }
}


/*function formatMasters( masterList ){
	//var masterData = new Array();
	try{
	 	masterList.forEach(function ( master ) {
	 		console.log(master);
	 		master.field.forEach(function ( name1 ) {
	 			var test = master._id;
	 			if (masterData.hasOwnProperty(test)) {
	 				 var item = {
				        "name": name1.name,
				        "id": name1.id
				    };
				    masterData[ test ].push(item);		
				    
			  	}else{
			  		masterData[ test ] = new Array();
			  		 var item = {
				        "name": name1.name,
				        "id": name1.id
				    };
				    masterData[ test ].push(item);
				    //masterData[test] = test1;
				    
			  	}
	 			
		    });
	    });
	    
	    //return masterData;
    }catch (exception){
    	return false;
    }
}*/
module.exports = mongoose.model('Master', MasterSchema);
