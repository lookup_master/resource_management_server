'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Master = require('../models/master');
/**
 * Billing Schema
 */
var BillingSchema = new mongoose.Schema({
  	resource_name: {
  	 	'name':String,'id':String
  	},
  	sdm: { 
  		'name':String,'id':String
  	},
  	customer: { 
  		'name':String,'id':String
  	},
  	project: { 
  		'name':String,'id':String
  	},
  	work_type: { 
  		'name':String,'id':String
  	},
  	billing_mode: {
  	     'name':String,'id':String
  	},
  	technology: {
  		'name':String,'id':String
  	},
  	date: { 
  		type: Date, default: Date.now
  	},
  	task: { 
  		type: String
  	},
  	status: { 
  		'name':String,'id':String
  	},
  	month: { 
  		type: String  
  	},
  	year: { 
  		type: String
  	},
    week: { 
      type: String
    },
  	hours: { 
  		type: Number
  	},
    invoice: { 
      type: Boolean
    },
    build_hours: { 
      type: Number
    },
  	comments: { 
  		type: String
  	},
    soft_delete: { 
        type: Number, 
        default:0
    },
  	created_on: { 
  		type: Date, 
      default: Date.now 
  	}

});


/*************************************************************************
 Function name  : monthlyBilling()
 parameters     : month, year, callback
 function call  : formatBilling
 Purpose        : This function is used for get monthly billing for resources
 *************************************************************************/

BillingSchema.statics.monthlyBilling = function(month, year, callback) {
	try{
	 	this
	 		.aggregate( 
	 			{ $match: { 'month' : month, 'year' : year  } },
			    { $group: { _id: '$resource_name.id', total: { $sum: '$hours' },
							field: { "$addToSet" : { "resource" : "$resource_name"}}}}, 
		    	function (err, billingsList) {
		    		if( billingsList ){
			    		formatBilling( billingsList , callback);
			    	}else{
			    		callback(err, false);
		    		}
			    }
			);
	} catch (exception) {
        return callback(null, true);SS
    }
};


/**
* format billing list
**/
function formatBilling( billingsList , callback){
    var billingData = new Array();
    try{
        Master
            .find(
                    {'type':'resource', 'soft_delete' : false},
                    {'name':true}
                )
            .exec(function(err, resourceList ) {
                var resourceIds = [];
                if(err) return false;
                billingsList.forEach(function ( billing ) {
                    billingData.push({
                        resource_name: billing.field[0].resource,
                        actual_efforts: billing.total
                    });
                    resourceIds.push(billing.field[0].resource.id);
                    
                });

                resourceList.forEach(function ( resource ) {
                    if( resourceIds.indexOf(resource.id) == -1){
                        billingData.push({
                            resource_name:{ id : resource._id, name : resource.name },
                            actual_efforts: 0
                        });
                    }
                });        
                callback(null, billingData);
            });
    } catch (exception) {
        return callback(null, true);
    }
}

module.exports = mongoose.model('Billing', BillingSchema);
