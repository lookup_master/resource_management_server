'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    tasks= require('../controllers/tasks.js'),
    assert = require('assert');

/**
 * Globals
 */
var user;
var article;

/**
 * Test Suites
 */
describe('<Unit Test>', function() {
    describe('Look up Master  :', function() {
        it('should be able to return tasks', function(done) {
            tasks.list(function(err,res){
                assert.equal(res.length > 0, true);
            });
            done();
        });
        it('should be able to save tasks', function(done) {
            var param = {
                "resource_name": "Afzal Khan",
                "sdm": "Rakteem B",
                "customer": "DriveSafe Colorado",
                "project": "Be Driving America"
            };
            tasks.create(param,function(err,res){
                console.log(res);
                assert.equal(res.resource_name , 'Afzal Khan');
            });
            done();
        });
    });
});
